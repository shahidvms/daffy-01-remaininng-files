Michael Issa

Woodbury, MN 55129 | (651) 757-0891 | michaelissa6@gmail.com

EDUCATION
Bachelors of Science in Recreation & Sports Management
St. Cloud State University, St. Cloud, MN (SCSU)
Minor in Marketing & Management
Professional Selling Certification
Dean s list Fall 2019
Cumulative GPA 3.77
Associate of Arts Degree
Inver Hills Community College, Inver Grove Heights, MN

August 2021

May 2018

PROFESSIONAL EXPERIENCE
Front Desk
Planet Fitness, St. Cloud, MN
December 2020-Present
Earned the achievement of top part time salesperson in the month of January demonstrating
competitiveness and drive to be the best
Cooperated as a team to ensure the cleanliness of the facility as a whole especially during these
unprecedented times
Assisted members with concerns in a professional and kind manner to demonstrate extraordinary customer
service
Marketing Intern
St. Cloud State University, St. Cloud, MN
October 2019 - March 2020
Collected game play statistics and performed data entry to update the SCSU Athletics Departments
website for a seamless end user experience
Proposed athlete photographs for department s marketing campaign which resulted in an average of 100
likes per social media post
Presented statistical data to coach and staff through effective interpersonal communication to prioritize and
facilitate game time decision making
Teller
Falcon National Bank, St. Cloud, MN
August 2019 - January 2020
Executed financial transactions for business clients averaging $80,000 per shift using the FISERV program
Addressed client concerns through in-person and virtual communication resulting in a customer first
experience
Performed closing procedures through strong attention to detail demonstrating trustworthiness and
reliability
Manager
Little Chopstix, Woodbury, MN
September 2015 - August 2019
Recognized for dedication to the organization by internal promotion from delivery driver up to manager
Provided real time advise on prioritization of tasks to a team of 3 staff members per shift leading to a
harmonious workplace environment
Resolved customer concerns and complaints in a timely manner demonstrating strong interpersonal
communication skills in order to provide a positive guest experience

ACADEMIC EXPERIENCE
Professional Selling Competition, SCSU
July 2020
Recipient of second place among 50 fellow students in recognition of effective selling strategies
Practicum, SCSU
August 2019 - May 2020
Volunteered 100 hours to perform various professional development activities in sports management and
marketing
Sales Management Simulation Competition, SCSU
December 2020
Received first place in the competition, specifically in profitability, sales volume, and market share

�

-----END OF RESUME-----

Resume Title: Marketing - General

Name: Michael Issa
Email: michaelissa6@gmail.com
Phone:
Location: Saint Paul-MN-United States-55129

Work History

Job Title: Front Desk 12/01/2020 - Present
Company Name: Planet Fitness

Job Title: Marketing Intern 10/01/2019 - 03/31/2020
Company Name: St. Cloud State University

Job Title: Teller 08/01/2019 - 01/31/2020
Company Name: Falcon National Bank

Job Title: Manager 09/01/2015 - 08/01/2019
Company Name: Little Chopstix

Education

School: St. Cloud State University, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Inver Hills Community College, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title:
Most Recent Pay: USD per
Managed Others:
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 2/18/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Hospitality - Hotel

Downloaded Resume from CareerBuilder
R2S5QT5Z4VNDX2KQM35


