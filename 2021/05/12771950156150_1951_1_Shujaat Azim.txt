Shujaat S. Azim
Full Stack Software Developer
Address: Potomac, MD, 20854
Phone: (240) 328-0494
E-mail: shujaat.azim@gmail.com

LinkedIn: www.linkedin.com/in/shujaatazim
Website: www.shujaatazim.com
Blog: https://dev.to/shujaatazim

Professional Summary
I am an enthusiastic full stack software developer with a background in, and passion for, technology, medicine, and
finance. I am offering strong skills in programming & development, leadership, teamwork, communication,
organization, self-motivation, and presentation - all gained from first-hand experience. I am a seasoned professional
with a diverse background in many fields, which enables me to quickly adopt and master new technologies while
working in both team and self-directed settings. I am always eager to continue learning and improving.

Employment History (full history available on website)
February 2020 April 2020

Software Engineer
Practical Solutions Inc, Washington, DC
Functioned as primary software engineer, 100% responsible for website and other
in-house projects entirely.
Adjusted design parameters of the company website to boost performance and
incorporate newer technologies such as React.
Designed and built in-house employee hour tracker app, using React and AWS,
completely replacing previous, outdated one.
Designed creative marketing campaigns with the team, including using Microsoft
Partners referrals, in-person demonstrations, and mass email.

September 2017 April 2019

Associate
Steward Partners, Bethesda, MD
Demonstrated proficiency in financial analysis by actively assisting financial advisors
with real-time simulations.
Planned and participated in bi-yearly financial advisor acquisition events,
onboarding new advisor teams and their 100+ client rolls.

June 2014 September 2017

Relationship Banker
M&T Bank, Potomac, MD
Promoted products/services to over 20 clients daily in order to consistently achieve
sales targets and increase engagement.
Built and maintained a personal client list by fostering trust-based relationships, and
became point-of-contact to over 60 clients/families.
Ensured branch efficiency by taking 100% ownership of the vault and ATM,
personally responsible for over $250,000 at any given time.

�Featured Projects (full list on GitHub and website)
Parley - An app that invites users to have a quick chat with someone whom they fundamentally disagree
with about a certain, specific topic.
Purpose: to demonstrate deep knowledge in React and Rails, and to help solve the problem of
opinionated entrenchment by fostering understanding of opposing sides.
Built the front end using React and Bootstrap styling, and the backend with Rails and custom REST
architecture.
https://github.com/ShujaatAzim/Parley-Frontend
https://github.com/ShujaatAzim/Parley-Backend
TweedleDoo - An app that invites users to create multiple small, specific to-do lists rather than one big list
in order to foster completion.
Purpose: to demonstrate my knowledge in multiple technologies, including brand new ones, and my
ability to use the, together in a single app.
Built the front end with React and Semantic UI styling, and the backend with Rails as an API to
dramatically reduce bloat.
https://github.com/ShujaatAzim/TweedleDoo-Frontend
https://github.com/ShujaatAzim/TweedleDoo-Backend

Skills
React JS (including libraries such as Redux, RecoilJS, and more)
Ruby on Rails (as both JSON API and full webapp)
Programming Languages (JavaScript, Ruby, HTML/CSS)
Testing (Unit testing, A/B testing, and libraries such as Jest and RSpec)
Databases (SQLite, PostgreSQL, GraphQL, REST API, Firebase)
Design (Material UI, Semantic UI, Bootstrap, Bulma, and more)

Education
May 2019 August 2019

Full Stack Development Bootcamp

January 2007 December 2011

Bachelor of Science: Neurobiology/Physiology

Flatiron School - Washington, DC
Immersive, in-person track.

University Of Maryland - College Park, MD

�

-----END OF RESUME-----

Resume Title: Technology - Developer, Programmer, Engineer

Name: Shujaat Azim
Email: shujaat.azim@gmail.com
Phone: true
Location: Potomac-MD-United States-20854

Work History

Job Title: Software Engineer 02/01/2020 - 04/30/2020
Company Name: Practical Solutions Inc

Job Title: Associate 09/01/2017 - 04/30/2019
Company Name: Steward Partners

Job Title: Relationship Banker 06/01/2014 - 09/01/2017
Company Name: M&T Bank

Education

School: true, Major:
Degree: None, Graduation Date:

School: true, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Flatiron School, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/2/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
R2R1HG77VKF2KV5N394


