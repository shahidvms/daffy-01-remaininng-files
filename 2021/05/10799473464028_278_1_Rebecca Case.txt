Rebecca (Ferguson) Case
Experience:
Mentor Coach Specialist
Fairbanks Native Association | Fairbanks, AK | Nov 2019 Observe, assess, and supervise students

Facilitate discussions between classrooms on a weekly basis

Work with sta across the Head Start program, filling in when and
where needed

Junior Instructional Designer
The Mission Continues | St. Louis, MO | May 2018 - Nov 2019
Designed, developed, and redeveloped learning materials and
sessions for three leadership programs

Oversaw and managed the Learning Management System

Collaborated with Subject Matter Experts in developing content
and prepping facilitators on a variety of topics

English Assessment Associate
ePerformax | Remote | Oct 2017 - Apr 2018
Analyzed recordings of English Language Learners

Strengthened speakers content and delivery by noting topics like
pacing grammar usage, mispronunciations, and others

Wrote reports based on findings in recordings

Contact:
Email: becca_f@sbcglobal.net

Phone: (618) 973-2017

Portfolio

Skills:
Throughout my work experience, I have
learned many skills including:

Microsoft O ce Suite

Particularly in Excel, PowerPoint,
and Publisher

Adobe Creative Suite

Particularly in Adobe InDesgin and
Illustrator

Work with Adobe Captivate

Facilitation and communication skills

Working independently as well as
collaboratively in a team setting

Office Assistant at The Mission Continues
Kforce | St. Louis, MO | May 2017 - Apr 2018

Education:

Ensured program participants and volunteers were set up for
success by connecting to them to appropriate sta nationwide

Analyzed financial statements to ensure accuracy of information

Worked with sta interdepartmentally on varying projects

Southwest Baptist University
Bachelor s of Arts, Writing
(minor in Business Administration)
Bolivar, MO | May 2016

Foreign English Teacher
Brighton English School | Ansan, South Korea | Aug 2016 Feb 2017
Created and organized lesson plans and presented them to
students daily with weekly schedules

Explained complex, foreign materials daily

Revised lessons based on students verbal and non-verbal cues to
ensure maximum engagement and retainment

In college I worked as a writer and editor for my
university s newspaper, and I was an intern with a
literary agent.

Certificates:
Train the Trainer
International Association for
Continuing Education |Oct 2018

�

-----END OF RESUME-----

Resume Title: HR - Training and Development

Name: Rebecca Case
Email: beccaferguson94@gmail.com
Phone: true
Location: Fairbanks-AK-United States-99707

Work History

Job Title: Mentor Coach Specialist 11/01/2019 - 03/21/2021
Company Name: Fairbanks Native Association

Job Title: Junior Instructional Designer 05/01/2018 - 11/01/2019
Company Name: The Mission Continues

Job Title: English Assessment Associate 10/01/2017 - 04/30/2018
Company Name: ePerformax

Education

School: Southwest Baptist University; Bolivar, MO, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 3/20/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
General Business
Supply Chain
Business Development
Consultant
Strategy - Planning
Telecommunications
BPO-KPO

Downloaded Resume from CareerBuilder
R2R8CS72DPB23RSQL4D


