Keith Beise
13600 Commerce Blvd. Apt. 354A, Rogers, MN 55340
keithbeise@gmail.com 612.816.8489
Summary:

MBA Candidate at Martin J. Whitman School of Management at Syracuse University

Strong communicator who works well collaborating across teams, but also very independently productive

More than 10 years of experience leading social media efforts for followings of more than 8 million people

Over nine years of public relations experience resulting in more than 400 million earned media impressions

Creative personality with a track record of successfully designing campaigns and content that encourage audience
interaction across a vast variety of channels

Calm demeanor with high productivity under strict and high-pressure deadlines

A positive, Marine Corps trained, leader who strives to provide a structure that ensures objective success

Education:

University of Minnesota Twin Cities Graduated Dec. 2011
Professional Strategic Communications: PR Emphasis (Bachelors)
Technical Skills:

Media relations and on-camera interviews

C-Suite reporting and executive speech writing

Strategic planning

News releases

Copywriting

Social media planning, posting and targeting

Digital advertising, including Facebook Ads Manager and Google Adwords

Proficient in sponsorship opportunities

Short and long-form content production for both print and digital outlets

AP Style

Google Analytics

Data reporting

SEO/SEM

CRM - Sitecore, CivicLive

Website accessibility

Adobe Suite

Full Microsoft Office product line

Work Experience:
Feed My Starving Children (August 2020 January 2021)
Senior Content Specialist

Create content for all FMSC social media channels

Monitor and respond to all comments across all FMSC outlets

Write copy for FMSC emails

Design graphics for FMSC's website, social and emails

Advise FMSC executives on media speaking points

Official FMSC photographer

-More-

�City of New Hope, Minnesota (July 2018 March 2020)
Communications Coordinator
Public Information Officer for the city of New Hope, Minnesota. Roles include:

Wrote, edited and produced all city publications including monthly and quarterly newsletters and triannual parks

and recreation magazines

Managed all aspects of the city s website, newhopemn.gov, including content, SEO and advertising

Operated all New Hope social media channels

Administered all New Hope public relations

Official city photographer, including drone photography

3M (March 2017 July 2018)
Digital Community Manager (on contract)

Managed social media content creation, curation and publication for 3M s Post-it, Scotch, Nexcare, Ace and Futuro
brands

Produced annual social media strategies, including content, timing and budget

Analyzed digital data and reporting to brand managers on results, trends and key takeaways

Listened across all available digital channels for engagement opportunities

Assisted in recommendations for SEO and SEM terminology

Starkey Hearing Foundation (September 2015 January 2017)
Digital Media Associate

Worked as the primary writer for all Starkey Hearing Foundation marketing materials

Editor-in-chief of 'So the World May Hear' magazine

Monitored and updated all Foundation social media platforms

Redesigned and updated content for starkeyhearingfoundation.org

Managed Google Analytics and Google Adwords to improve SEO

Communicated with Foundation's public relations firm for all media relations

Be The Match (March 2015 September 2015)
Interactive Project Specialist

Kept Be The Match on top of the edge of non-profit social media performance

Monitored and updated all social media platforms including Twitter, Facebook, Instagram, Google+ and LinkedIn

Updated bethematch.org with new content, adjusting to be in line with new brand guidelines

Assisted business groups in developing and promoting sponsorship opportunities

Minnesota Twins (January 2012 March 2015)
Digital Communications Manager

Monitored and updated all social media platforms for the Twins to keep fans up to date about all team information

Planned, promoted and executed social media promotions to encourage fan interaction and media coverage

Created, edited and delivered content to Twinsbaseball.com

Researched and implemented the latest digital trends including mobile app development, beacon technology and
utilizing physical spaces to encourage social media interaction with fans

Wrote, edited and distributed news releases to the Twin Cities media

United States Marine Corps (September 2004 September 2009)
I-Level Communications/Navigations Systems Technician

Worked as a component-level technician on Harrier communication and navigation systems

Deployed in 2007 on the 31st Marine Expeditionary Unit, certifying over 150 pieces of mission-ready gear

Worked in numerous leadership positions, and achieved the rank of sergeant in just over four years

Furthered leadership abilities by spending the final year of service as squadron clerk

�

-----END OF RESUME-----

Resume Title: Advertising/Public Relations - Media

Name: Keith Beise
Email: keithbeise@gmail.com
Phone: true
Location: Hamel-MN-United States-55340

Work History

Job Title: Senior Content Specialist 08/01/2020 - 01/31/2021
Company Name: Feed My Starving Children

Job Title: Official FMSC photographer 07/01/2018 - 03/31/2020
Company Name: More

Job Title: Digital Community Manager 03/01/2017 - 07/01/2018
Company Name: 3M

Job Title: Digital Media Associate 09/01/2015 - 01/31/2017
Company Name: Starkey Hearing Foundation

Job Title: Interactive Project Specialist 03/01/2015 - 09/01/2015
Company Name: Be The Match

Job Title: Digital Communications Manager 01/01/2012 - 03/01/2015
Company Name: Minnesota Twins

Job Title: I-Level Communications/Navigations Systems Technician 09/01/2004 - 09/30/2009
Company Name: United States Marine Corps

Education

School: University of Minnesota, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/3/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Arts - Culture

Downloaded Resume from CareerBuilder
R2R6B86KHW26FS9JDMN


