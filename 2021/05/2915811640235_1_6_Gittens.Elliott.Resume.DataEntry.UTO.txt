                             
               28 Jackson Avenue                     Cell Phone (607) 341-8178
               Apt.3                                 improvedincome@gmail.com
               Endicott, NY 13760




               ELLIOTT GITTENS
_________________________________________________________________________________________________________________________________________

               Objective: A position as a Remote Customer Service Representative 


               Summary Of
               Qualifications:  Microsoft Word, Microsoft Excel, Data Entry, Typing


               Education:       10/2007 - 10/2008          New England Institute of Technology               Warwick, R.I.
                          
                                * Pursued an associate degree in Electrical Technology
                                * Obtained my OSHA Card

                                9/1995 - 5/1999            Long Island University,Brooklyn Campus            Brooklyn, N.Y.

                                * Major:  Social Anthropology

               
               
                              
               Professional    9/2020 - 12/2020
               Experience      Customer Service Rep.         MMC Conduent                                 Endicott, N.Y.
                               
                               * Took toll bil customer calls
                               * Transferred T.O.R.'s and Dispute C's to their accounts
                               * Enrolled toll bill customers to E-Z Pass
                               * Transferred customers to collections and Settlement
                               * Answered customers general questions regarding E-Z Pass



                                5/11/2020 - 9/2020             Wayfare                                     Endicott, N.Y.
                                Commercial Cleaner

                                * Used mop and mop bucket,vacuum cleaner
                                * Cleaned and maintained lab,pharmacy,walk-in and commons
                                * Disposed trash including recyclables
                                * Maintained and cleaned first level of hospital
                                                              
                                
                                9/8/2016 - 4/15/2020             Lowe's                                        Vestal, N.Y.
                                Receiver/Stocker
                               
                                * Used pallet jack to deliver products to various store departments
                                * Overseen Paint and Flooring Department
                                * Stocked Paint and Flooring Department Shelves
                                * Maintained Paint and Flooring Department cleanliness by sweeping
                                * Checked for product availabilty by using Genesis Software


                                8/1/2018 - 7/1/2019           New York State Insurance Fund                  Endicott, N.Y.

                                * Sorted the mail into different categories that consisted of
                                  case manager lookups,DCC lookups and bill lookups
                                * Delivered the mail to the case manager units
                                * Sent the mail through the Neopost machine to be weighed and stamped


                                                          

                               
                                5/15/2015 - 5/22/2015              Leviton                                   Melville, N.Y.
                                Mail Room Clerk

                                * Delivered and picked up the mail from the various departments
                                * Processed mail to be sent out UPS
                                * Used data entry to input the UPS shipments into the system
                                * Sorted and delivered the inter office mail to the various departments
                                * Handled the Neopost machine which had hand feed and automatic
                                * Operated the Hasler IM 5000 which weighed, sealed, and stamped the mail

                            
                             
                                1/2014 - 4/2014                    Liberty Health Advantage                  Melville, N.Y.
                                Mail Room Clerk

                                * Processed the returned mail into the Microsoft Excel spreadsheet
                                * Processed the undelivered mail into the Microsoft Excel spreadsheet
                                * Sorted and delivered the inter office mail to the various departments
                                * Processed bulk member mailings
                                * Processed the outgoing mail down to the lower level
                                * Picked up the mail at the post office and building mail room daily
                                * Responded to member requests for plan materials                                                            


                                                            
               Languages          English

               References         Furnished Upon Request

               Extracurricular
               Activities         creative writing, blogging, music, animation

               Accrediations      degree in associate arts, certificate of achievement, OSHA Card,
                                  Order Picker Lift, Narrow Aisle Extended Lift       




                                














