Timothy R Hogston			     
Utility CNC Machinist 	
404 Woodlawn Ave  Aurora. IN 47001
Phone: 812-577-7339
Email 1: hogstonimothy@live.com 
Email 2: hogstontimothy@gmail.com


Objective:
Secure a Challenging position within your Company as a Utility Machinist CNC using my 38 years of Exctensive Experience & Exceptional Knowledge Skills. I'm capable of machining Complex Componets in the Aero Space MRO Repairs, Military Contract Parts, Machine Tool Industry Parts, High Speed Packaging & Automotive Machine Manufacturing procedures.

Abilities:
Supervision, Leadman, Close Tolerence Machining, Machine Training, Manual & CMM Inspection, GT&D Knowlege & Blue Print Reading, Machine Process Planing, Machine Setup, Machine Tooling Selection, G&M Code Programming & Editing at CNC Machine Control.
 
Skills:
Operation of Toshiba, Deckel Maho, Mazak, Haas, Fadal, Johnford, Anayak CNC 5 Axis Machines. CNC Machining on HBM, HMC, VMC, VTL machines. Manual Machining on Lathes, Mills, Grinders.

Experince:
Standard Aero 2013-2020 - CNC Machinist
 Setup 5 Axis CNC Machine
 Match Part Serial Number & Work Router
 Identify Material being Machined
 Confirm Engineers Requirements
 Indicate or Probe Fixture/Part Alignment
 Program & Edit as needed
 Select Tooling used for Repair
 Machined Castings, Weldments, Titanium, Hastelloy, Stainless Steel,
 Inconel, Aluminum Billet, Composites
 Machine Repairs on Aviation Parts
 Machine Repairs on Military Parts
 Machine Fixturing for New Part Repairs
 Inspect Machined Part Repair Dimensions
 Maintenance of Machine & Tooling
 CMM Inspection Operator

TSS Technologies 2003-2013 - Supervisor, Leadman, CNC Machinist
 Second Shift Supervision of 65 Machinist
 Machine Load & Machinist Scheduling
 Handle Machine Tooling & Programming Issues
 Leadman Assist Machinist Understanding Processes
 Setup 5 Axis CNC Machine or Horizontal Boring Mill
 Indicate or Probe Fixture/Part Alignment
 Program & Edit as needed
 Machined Castings, Weldments, Titanium, Hastelloy, Stainless Steel,
 Inconel, Aluminum Billet, Composites
 Select Proper Tooling
 Inspect Machined Part Dimensions
 Operation of Overhead Cranes, Load/Unload Machined Part

RA Jones 1990-2003 - CNC Machinist
 Setup 4 Axis CNC HMC 10 Pallet Machine
 Select Priority Work Router
 Machined Castings, Weldments, Stainless Steel, Aluminum Billet
 Confirm Engineers Requirements
 Indicate or Probe Fixture/Part Alignment
 Prove out Program & Edit
 Select Proper Tooling
 Build Moduler Fixturing for Next Job
 Inspect Machined Part Dimensions
 Maintenance of Machine & Tooling
                                                                                                            
Education:
South Dearborn High School
5770 Highlander Place
Aurora, IN 47001
Phone: (812) 926-3772

Southeastern Indiana Career Center
901 W US Hwy 50
Versailles, IN 47042
Phone: 812-689-5253

Manchester Elementry School
Manchester, IN 46962
Phone: 812-926-1140


