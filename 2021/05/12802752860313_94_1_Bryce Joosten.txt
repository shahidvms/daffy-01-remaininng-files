Bryce Joosten
IT Coordinator & Esports Coach
Cedarburg, WI 53012
brycejoosten@yahoo.com
7153151617
Willing to relocate: Anywhere
Authorized to work in the US for any employer

Work Experience
IT Coordinator

Cristo Rey Jesuit High School - Milwaukee, WI
July 2020 to Present
Coordinator and administrator of all Information Technology Services
Windows Admin & Google Workplace Admin
Designs and implements new processes and strategies
Creates and maintains all best practices
Supports both the academic and administrative departments of the school
Manages 400+ Chrome devices, 100+ Windows devices, and various other technology such as Apple
and Android devices, network devices, printers, and projectors
Administrative and Business Operations
Esports Coach

IT Specialist

PRE/3 Real Estate Management - Brookfield, WI
December 2019 to July 2020

Microsoft 365 Administration
Resolving technology issues in person at a central office and over the phone for offsite properties
Manage cell phone accounts and phone ordering/deployment
Maintain IT help desk and use of ticketing system
VoIP phone system
Chromebook deployment and Chrome administration
Active Directory administration
Building apps and flows via Microsoft Power Apps and Microsoft Power Automate (flow)
Troubleshooting Canon MF series printers and scanners
Ordering and setup of new technologies - desktops,Chromebooks, iPads
Windows 10 deployment
PC hardware repair
TeamViewer
VMware
IOS
MacOS

IT Field Manager

Riiser Fuels LLC - Milwaukee, WI

�August 2018 to December 2019
Responsible for resolving technical issues at retail locations, administrative offices, and with mobile
users
Supported users and sites via remote technologies and/or physically getting onsite
Provided helpdesk support 24/7 via Zendesk ticketing system, emails, texts and voice communications
as necessary
Travel between 70 locations to resolve and implement IT technologies
Developed policies and procedures for devices and programs regarding tech support (for techs) or
usage (by end-users)
Monitored online and cloud-based systems as applicable to keep systems operating at best
performance
Worked with HR/Operations/Maintenance as necessary to complete technical needs associated to
projects
Integrated with 3rd party vendors regarding installations of systems and technology, testing and
setting up systems to Riser standard
Wayne and Gilbarco Fuel Dispensers
Installation, maintenance, and monitoring of security systems, DVRs,NVRs, and security cameras (IP
and Analog)

Store Manager, Multiple Locations
Riiser Fuels LLC - Wausau, WI
September 2017 to August 2018

Provide customer service by greeting and assisting guests, and responding to guests inquiries and
complaints.
Do paperwork daily and work within the company BackOffice application
Inventory stock and reorder when inventory drops to a specified level.
Assign team members to specific duties.
Create and maintain a safe work environment for guests and team members
Troubleshoot instore issues before forwarding on to maintenance or IT

Customer Service Manager
Wal-Mart - Wisconsin Rapids, WI
August 2014 to September 2017

Provide high quality customer service
Work closely with store assistant manager and store manager
Provide support in other departments and areas of the store
Manage cashier money orders and deposits
Refine important communication skills

Education
Bachelors in Progress in Cybersecurity & Management
Purdue University Global
Present

�Skills
Desktop Support
Network Support
Information Technology
IT Specialist
Help Desk
Zendesk
Comptia
Cisco Meraki
Computer Repair
Kronos
Microsoft Office
Administrative Assistant
Cisco
Typing (10+ years)
Retail Management (3 years)
97 WPM typing speed
Retail Sales (5 years)
Excel (10+ years)
Microsoft Word (10+ years)
Supervising Experience (3 years)
Marketing
PCI
Installation
Maintenance
Equipment Repair
Google Suite
Analysis Skills (2 years)
TCP/IP (3 years)
Print Servers (3 years)
Video Conferencing (3 years)
Technology Services (4 years)
Technical Planning (3 years)
Active Directory (3 years)
Office Administration (3 years)
Chrome Admin (3 years)
Google Cloud Platform
Microsoft Outlook
Vendor Relationships

� Vendor Management
Network Administration (3 years)
Administration
Sales Experience
VoIP
Windows AD
Security System Experience
Policy Development
Leadership Experience
Management Experience
Forecasting
Management
IT Experience
CompTIA
Cisco Routers
Cisco ISE
Organizational Skills
Administrative Experience
VMWare
iOS
Mac OS
Software Testing
Operating Systems
Management
Adobe Illustrator
Management Experience
CRM Software
Time Management
Operations Management
DHCP
Ethernet
LAN
Linux
Microsoft Windows Server
VPN
WAN
Technical Support
Telecommunication (2 years)
Installation (2 years)
Research

� Microsoft Excel
Data Collection
Accounting
Food safety
Food & Beverage
Network Infrastructure
ServiceNow
Microsoft Exchange
Administrative and Business Operations
Power BI

Certifications and Licenses
Driver's License

Assessments
Organizational Skills Highly Proficient
July 2018

Measures a candidate's ability to arrange and manage files or records using a set of rules.
Full results: Highly Proficient

Technical Support Highly Proficient
November 2019

Performing software, hardware, and network operations.
Full results: Highly Proficient

Filing & Organization Highly Proficient
July 2018

Measures a candidate's ability to arrange and manage files or records using a set of rules.
Full results: Highly Proficient

Technical Support Expert
April 2020

Performing software, hardware, and network operations.
Full results: Expert

Scheduling Expert
July 2018

Measures a candidate's ability to cross-reference agendas and itineraries to avoid conflicts when
creating schedules.
Full results: Expert

Management & leadership skills: Planning & execution Highly Proficient
January 2021

�Planning and managing resources to accomplish organizational goals
Full results: Highly Proficient

Security guard skills Highly Proficient
March 2021

Assessing risks, enforcing security standards, and handling complaints
Full results: Highly Proficient
Indeed Assessments provides skills tests that are not indicative of a license or certification, or continued
development in any professional field.

Additional Information

Advanced knowledge of both Microsoft and Apple operating systems
Passion for a knowledge of hardware and physical device repair
Strong and fluent communicator
Flexible - Willing to take on any challenge or role and achieve high success
The ability to understand problems and solutions from multiple perspectives

�

-----END OF RESUME-----

Resume Title: Technology

Name: Bryce Joosten
Email: brycejoosten@gmail.com
Phone: true
Location: Cedarburg-WI-United States-53012

Work History

Job Title: IT Coordinator 07/01/2020 - Present
Company Name: Cristo Rey Jesuit High School; PRE/3 Real Estate Management

Job Title: IT Field Manager 08/01/2018 - 12/31/2019
Company Name: Riiser Fuels LLC

Job Title: Store Manager, Multiple Locations 09/01/2017 - 08/01/2018
Company Name: Riiser Fuels LLC

Job Title: Customer Service Manager 08/01/2014 - 09/01/2017
Company Name: Wal-Mart

Education

School: Purdue University Global, Major:
Degree: Bachelor's Degree, Graduation Date:

School: true, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/11/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
R338BP7308YSLJSQHNW


