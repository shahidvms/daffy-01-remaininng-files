STEVEN TORGERSON
Steven.Torgerson@gmail.com| (702) 285-6600 |linkedin.com/in/steven-torgerson
1839 Vista Pointe Ave, Henderson, NV 89012



PROFILE
Creative, Adaptable, Motivated, People first manager who is looking to grow in my career and find
new avenues. I have been in commercial and entrepreneurial retail for 20 years. Looking to grow the
scope and opportunity of the market.



EXPERIENCE
November 2015- Present
Polo Ralph Lauren- Operations Manager/Merchandising Manager
Manage high volumes in commercial retail. Develop and assist facilitate high unit sales and drive
productivity through staff volumes as high as 130. Driving top line sales through measurables,
coaching and development of talent, interacting and requesting resources from senior management.
Problem solving.

August 2013- November 2015
Old Navy- ASM/ Lead Merchant
Overseeing Door-to-Floor Processing and Merchandising. Development of staff into management
positions. Driving top line sales through measurables, coaching and development of talent,
interacting and requesting resources from senior management.




January 2010- June 2013
Corner Store Furniture -Owner/ Buyer/Merchandiser/ Logistics Manager
Overseeing all aspects of opening and merchandising a new idea store. Hiring and managing sales
staff to drive business. Buying merchandise direct from manufacturers that would best fit the
customer base. Developing and maintain a website and online presence through social media and SEO.




EDUCATION
University of Nevada Las Vegas- Bachelors of Science Finance and Real Estate



KEY SKILLS AND CHARACTERISTICS
* Critical Thinking
* Adaptability
* Leadership
* Resourcefulness
* Creative Thinking
* Problem Solving



Page 1