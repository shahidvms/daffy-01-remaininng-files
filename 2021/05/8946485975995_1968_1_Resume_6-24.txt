﻿Brittany Galindo
465 Saw Mill Rd, West Haven, CT 06516(203)685-6785 brittruffin@yahoo.com 




Experience
Spotless Laundromat, West Haven, Ct 
Manager, August 2014 - present
* Prepare invoices, document accounts receivable, and track business expenses using Quickbooks. 
* In charge of payroll and scheduling shifts
* Customer service
* Data entry
Owner: Dave Breton
(203)988-2224


Yale New Haven Hospital, New Haven, Ct 
CSS Technician I, February 2020 - October 2020
* Assemble a range from simple sets of several instruments to very complex sets over ninety instruments.
* Inspect, assemble and determine the correct method and packaging for sterilization such as but not limited to steam, Sterrad, Steris, ETO, and high level disinfecting scope reprocessors.
* Assemble and select the correct sterilizing method for a variety of delicate equipment such as flexible Bronchoscopes, ENT Scopes, Speech Pathology Scopes, Cystoscopes, and other flexible and rigid endoscopes.
* Accommodates special and emergent requirements through consultation and coordination with the medical surgical professional staff.
Manager: Cynthia Laurello
(203)688-2214


United States Postal Service, Monroe, Ct
Postal Support Employee, May 2018 - March 2019
* Sorting and preparing Mail for distribution
* Hand sorting letters
* Preparing Mail to load into care your trucks
* Sorting parcels into the correct routes for delivery
* Loading and unloading trucks
Postmaster: Bill Evans
(203)261-3880




Education
Housatonic Community College, Bridgeport Ct
* Sterile Processing Technician 2019
* 24 hours of clinical 
   * Decontamination 
   * Package and prep
* 60-hour course 
Central High School, Bridgeport Ct 
* High School Diploma 2007


Certification 
Certified Sterile Processing and Distribution Technician
By CBSPD
ID#: 43482
Exam Date: 08/05/19


Skills
* Great verbal communication
* Organized 
* Understanding and retaining information
* Able to take on multiple tasks at once