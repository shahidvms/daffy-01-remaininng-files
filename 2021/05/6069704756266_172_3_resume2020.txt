Objective
I want to provide quality services.

Executive Summary
Hello, My name is Alphonzo Sykes. I have recently relocated to Belleville,IL and I am looking for immediate employment. I will offer customers the best methods, tools, planning and implementing of a successful experience, increase sales, and enhance customer satisfaction in any field of work.

First name 
Alphonzo
Middle name 
D
Last name 
Sykes
Address
Todd Ln
Country/Region United States
City 
Belleville
State/Region/Province 
Illinois
Zip/Postal code 
62221
Primary Phone 
6182075936
Email
alphonzosykesbusiness@gmail.com
The SSN should be in xxx-xx-xxxx format.
***-**-1314
Gender
Male
Race/Ethnicity 
Black or African American (Not Hispanic or Latino)
Desired Salary
Negotiable
Veteran Status 
NO
Certificates and Licenses

Alcohol Server Permit
Certified Customer Service Representative
Food Handler Certificate

Education history

Lincoln Land Community College
Graduation year 2009 G.E.D
Most Recent 
2010 Lincoln Land Community College
Major area of study 
COMPUTER SCIENCE
Degree OTHER

Work experience

Lawn Care
2019 - 2020
Responsibilities
Perform groundskeeping and building maintenance duties. Mow lawn either by hand or using a riding lawnmower. Cut lawn using hand, power or riding mower and trim and edge around walks, flower beds, and walls. Landscape by planting flowers, grass, shrubs, and bushes.

U.S Gas
CASHIER
2019 - 2019
Responsibilities 
Welcoming customers, answering their questions, helping them locate items, and providing advice or recommendations. Operating scanners, scales, cash registers, and other electronics. ... Accepting payments, ensuring all prices and quantities are accurate and proving a receipt to every customer.

Casey's General Store
COOK
2018 - 2019
Responsibilities
 Casey's General Stores provides freshly prepared foods, quality gasoline, and friendly service at every location. Guests can enjoy famous, made-from-scratch pizza, donuts, other assorted bakery items, and (at select stores) Casey's made-to-order sub sandwiches

OWNER/ PROMOTER
A-BOS ENT
Aug 2009 - Current
A promotional event is an occasion that draws attention to a particular product or products, such as a price decrease, or the chance to win a prize when buying the product. Promotional events for our retail clients are held at some of the top conference and event venues and include tours and catering.

BARTENDER
AFTER HOURS
Nov 2008 - Oct 2017
Prepare alcohol or non-alcohol beverages for bar and restaurant patrons.
Interact with customers, take orders and serve snacks and drinks.
Assess customers' needs and preferences and make recommendations.
Mix ingredients to prepare cocktails.
Plan and present bar menu.

Cook
Long John Silver �s
Jun 2008 - Nov 2008
Responsibilities
 Inspect food preparation and serving areas, Season and
cook food according to recipes, Weigh, measure, and mix ingredients,
Regulate temperature of ovens, broilers and grills, Portion, arrange, and
garnish food and Wash, peel, cut, and seed fruits and vegetables

Delivery Driver
God Fathers Pizza
May 2007 - Jan 2008
Responsibilities 
Collect money from customers, Provide Customer
Service, Record sales or delivery information, Deliver items, Turn in
receipts and money received and Read maps, and follow written and verbal
geographic directions 

Dietary Aide
Golden Moments Nursing Home
May 2006 - Oct 2006
Dietary Aides are responsible for the preparation and handling of food created by chefs. Depending on the job, additional duties may include serving meals to patients and residents, stocking inventory, recording meals served, monitoring food temperatures, cleaning the kitchen and washing dishes, cutlery and equipment.
Sales Representative

A-pac
Telemarketing
Jun 2002 - Dec 2003
Telemarketing (sometimes known as inside sales, or telesales in the UK and Ireland) is a method of direct marketing in which a salesperson solicits prospective customers to buy products or services, either over the phone or through a subsequent face to face or web conferencing ap.

Skills
Cooking, Customer Service, Customer Service, Data Entry, delivery, direct marketing, edge, Graphic Design, inside sales, inventory, Marketing, Mentoring, money, Nursing, Painting, phone, Project Management, Quick Learner, Read, recording, sales, supervisor, Telemarketing, Video Editing

If hired, can you provide proof that you are legally allowed to work in the United States? 
Yes
Are you able to answer telephones and take orders with or without reasonable accommodation? 
Yes
How much customer service experience do you have? 
3+ years

Availability
Anytime
Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, 
How many hours per week do you wish to work? 
40

References

Name
Cecilia Jackson 
DSP Alvin Eades Center
Phone Number 217 491 6520
Relationship co-worker

Name 
Nathan Orr 
Pathway Services Maintenance
Phone Number 217-883-8697
Relationship co-worker