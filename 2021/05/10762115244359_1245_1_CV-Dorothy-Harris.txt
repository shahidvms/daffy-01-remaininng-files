
Dorothy Harris
9839 Pebble Valley Ln.
Dallas, Tx 75217
214-534-5335
dorothyharris26@yahoo.com

EXPERIENCE
Staffmark, Richardson, Texas — Remote Customer Service Email Agent
November 2020 - PRESENT
Provide assistance to clients, Maintained up-to-date knowledge of customer accounts, Explain products and membership benefits, Confirm payments, refunds, etc. Resolve payment and order disputes, Assist customers with inquiries and complaints. 

Redmond and Greer Pharmacy Supply, Dallas, Texas— Customer Service
April 2019 - March 2019
Process customer and sales representative orders into the system. Answer inbound customer service calls pertaining to order status, delivery status, product inquiries. Place outbound calls to customers to provide information on orders. Update pharmacist license,  account information,  and sales representative information. Complete customer audits. Set up new accounts. Answer emails. Perform data entry for orders and customer information.

McElroy Metal, Sunnyvale, Texas — Administrative Assistant/Receptionist
September 2017 - April 2019
Sort Incoming orders, verify and confirm shipments, manage emails, answer inbound calls, keep daily record of shipments using excel. Deliver invoices and shipping statements to proper department to be processed.

EDUCATION
Bishop Lynch High School, Dallas, Tx — Diploma
August 2003 - May 2007


SKILLS
Data Entry, Administration, Email Support, Customer Service, Organizational Skills, Basic Computer, Basic Word Processing, Scheduling, Detail Oriented, Clerical, MS Office, Outlook, Salesforce. 




