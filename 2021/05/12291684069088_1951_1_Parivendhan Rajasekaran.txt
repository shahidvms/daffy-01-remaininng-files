Parivendhan Rajasekaran
rajasekar.pari@gmail.com
615-720-2028

SAP Certified Application Associate - SAP Transportation Management 9.5

13+ years of overall experience in which 4+ years in consulting as SAP TM Functional Consultant
One Implementation and roll out projects in SAP TM.
Worked on the Support Activities in SAP TM.
Good Working Experience in SAP TM & ABAP implementation, Support, and business requirement driven projects.
Experienced in BOPF, and ABAP.
Expertise in Business Processes of Manufacturing, Automobile, Consumer goods & Retail Industries
Adept in implementation, system integration, and post go-live support and in providing in-depth knowledge of end-to-end business processes related to Supply Chain and technology integration.
Worked in delivering blueprints, Solution Design documents and functional specifications for Transport Requirements to Manufacturing, Retails and logistic Companies.
Test Phase Planning (Unit/Assembly/Integration/UAT) involving test script creation & resource allocation.
Pro-active trouble-shooter, who can instantly identify business problems, formulates strategic plans, initiate changes and implement new processes in challenging and diverse environments.
Excellent communication and interpersonal skills and ability in troubleshooting and optimization.
Experienced in SAP TM & EWM Integration, EDI IDoc Processing, BOPF, BRF applications.
Have functional knowledge of SAP SD, MM, FI and EWM module.
Coordinate with internal teams the follow-up and update of the escalated calls.
Resolve incidents across the SAP platform & escalate when appropriate.
Ensure SLA s are met.
Experienced in the ServiceNow tool.
Sound experience understanding of SAP/ABAP Programming and BW/BI Objects and Reporting.

Role - SAP Function Consultant (TM) 01/2019 to Current
Nissan North America Franklin, USA

Responsibilities: -
Setting Up of TM S/4 HANA On premise Embedded, S/4HANA TM 1909
Optimizer Configuration including selection profile, planning profile, incompatibilities etc. Order
Tendering and Carrier Determination.
Freight Order Management, Forwarding Order Management, Logistics integration, Transportation
Network, Transportation Mode, Freight Agreement, Tendering, Cost Distribution.
Worked on Business Object Processing Framework (BOPF) and Transportation Charge Management Configuration.
Worked on defining Charge Calculation process by setting up Charge Profiles for Settlement, Charge Categories &amp; Charge Type, Freight Agreement Set up, Freight Agreement assignment to Carrier and Purchase Org, Calculation sheet creation with necessary Rate table and Scales.
Well Versed in Domestic inbound transportation, domestic outbound Transportation, Shippers Scenario(Basics), Logistic Integration, Charges, Cost Distribution and Freight and Forwarding Settlement.
Worked on defining Freight Settlement Profiles, Dispute Management and Carrier Invoice creation and postings.
Experienced on building interface for anything related to logistics going from or into SAP TM. This includes external and SAP.
Drove root cause analysis by conducting post- incident review meetings thereby preventing re-occurrence of systemic issues.
Prepared the configurational documents, training documents for Inbound, Outbound, and internal process.
Creation of Functional Specification document, Functional User Training documents, Smoke Testing & Upgrade Testing, Configuration, Customization etc.
Co-ordinated with technical guys to share the functionality of the requirement.
Resolved 100% Major incident by providing effective and rapid responses as per SLA and OLA timeframe.
Developed firm understanding of agile values and principles and scrum framework.

Prepared the configurational documents, training documents for Inbound, Outbound, and internal process.
Worked on the Agile/Scrum methodology.

Role - SAP TM Consultant 05/2016 to 12/2018
Corteva Wilmington, DE USA

Responsibilities: -
Involved in unit testing/integration testing for outbound, inbound Process.
Extensively Worked on BOPF Dependent objects.
Master Data setup, configuration, and customization of Business Partners, Org. Management, Transportation Network and Modes.
Worked on defining document types for OTR/DTR, Freight Unit, Freight Order/Booking Order, Freight Unit Building rules (FUBR), Planning Profiles, Carrier Selection settings, Automatic Freight Planning through Strategy setup, Carrier assignment based on Direct Shipment option
Creation and Editing of Freight Bookings, Pickup and Delivery Freight Orders, Freight Orders for customer self-delivery and pick-up.
Integration of SAP TM with EM, ERP and Legacy.
Good knowledge of Transportation Cockpit Planning, Forwarding Order Management, Freight Order Management, Settlement FSD, FWSD, Charge Management, Event Monitoring.
Participate in discussions with key stakeholders to analyze issues, support in identifying root cause.
Design, Configuring and Unit testing for developed objects.
Go-Live Readiness cutover activities and post go-live support.
Worked on the Agile/Scrum methodology.
Role - SAP Sr. BI ABAP Consultant 09/2011 to 04/2016
Mars Information Services Mount Olive, NJ

Responsibilities: -
Analyzed the existing process of multiple application areas of sale and distribution, Financial Accounting (AR/AP). Implemented the fixes for the bugs.
Worked on developing various RFC for orders creation and change and status for the custom requirements for the business.
Involved in the SAP Hana upgrade testing activities for SD and MM functional areas.

Updated code as per S4HANA standard to improve the performance of the Jobs.
Worked on the BI Performance optimization to improve the loads runtime.
Conducting Unit, Integration, and release testing to validate functionality.
Built custom extractors on top of SAP tables to create reconciliation report in BO.
Worked on the BADI s and BAPI.
Supported BI processes and solutions by analysis of business requirements.
Designed and developed queries to generate reports in BEx Analyzer to send data to tableaus reporting.
Worked on Publishing BEx Report to BO tool.
Worked on APD Analysis Process Designer to update the data from report to Target.
Extensively used Structures, cell definition, calculated key figures, restricted key figures, new selection, new formula, exceptions, and conditions.
Prepared and presented solution proposals to the leadership.
Ensure documentation of detailed specifications of design, test, and training programs.
Worked Closely with Business owners, stakeholders to create user requirement specification, functional and technical design documents.
Worked with test group and business users to perform unit testing, user acceptance.
Role - SAP BI ABAP Consultant 01/2009 to 09/2011
Momentive Performance Material
Momentive Performance Materials is a silicones business brings innovation to a wide range of industries, including automotive, electronics, personal care, consumer products, aerospace, and building and construction.

Responsibilities: -
Monitoring of daily loads and fixing failures.
Set up periodic Data loads from legacy system (flat files)
Maintenance of Process chains, Info packages for changes in schedules or repair loads.
Handling Month, quarter, and year end outage activities.
Resolving tickets raised by Users for the issues on data discrepancy on Reports, validating data with source systems or if application is not working or it is slow.
Knowledge transfer along with team members regarding technical guidance & support throughout project lifecycle.

Created Process chains for all jobs, which were scheduled directly through Info packages.
Performance initiatives were taken to improve performance of queries.
Created Queries, Web Templates, Views, and Workbooks using BEx.
Worked on enhancing standard SAP FI, SD, LO extractors with ABAP code at extractor level.
Worked on generic extractors, complex transformations, and ABAP code.
Worked on integrating of web applications to Enterprise Portal 6.0 by creating Folders, Roles, Roles assigning to end users, Pages, and publishing BW Bex in Portal.
Role: SAP Technical Consultant 01/2007 to 12/2010
ALSTOM PROJECTS INDIA PVT LTD

ABAP Responsibilities:
Developed Reorder level report and PR wise PO details Report.
Customized Result Analysis for Project Report.
Developed layout for printing PO amendment details which is used to show changed values in PO.
Created BDC programs to upload Vendor/Material data from legacy system to SAP system.

BW/BI Responsibilities:
Involved in ABAP coding while writing Start/End Routine and performing lookups to populate data for fields.
Monitoring the daily data loads and fixing the issue. Taking care of the support activities and handling the tickets.
Worked on variable exists ABAP code to derive currency exchange rates.
Created and Enhanced custom extractor based on business requirements.
Education
Bachelor Technology: Mechanical Engineering Technology 05/2006
Vellore Institute Technology (VIT) India

-----END OF RESUME-----

Resume Title: Technology - Developer, Programmer, Engineer

Name: Parivendhan Rajasekaran
Email: rajasekar.pari@gmail.com
Phone: true
Location: Wilmington-DE-United States-19895

Work History

Job Title: SAP Function Consultant (TM) 01/01/2019 - Present
Company Name: Nissan North America

Job Title: SAP TM Consultant 05/01/2016 - 12/31/2018
Company Name: Corteva

Job Title: SAP Sr. BI ABAP Consultant 09/01/2011 - 04/30/2016
Company Name: Mars Information Services

Job Title: SAP BI ABAP Consultant 01/01/2009 - 09/01/2011
Company Name: Momentive Performance Material

Job Title: SAP Technical Consultant 01/01/2007 - 12/31/2010
Company Name: ALSTOM PROJECTS INDIA PVT LTD

Education

School: Vellore Institute Technology (VIT) India, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/26/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
RD72VN65BXJ6MXXH7LJ


