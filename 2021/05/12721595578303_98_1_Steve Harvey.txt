Steve Harvey
748 South 7th Street (541) 621-2848
Lakeview, Or. 97630 steveharvey300@yahoo.com

Skills and Qualifications

Project Implementation Skills: Experienced in planning and implementing projects from start to finish; developing methods and procedures to accomplish defined goals with attention to detail; protocol; monitoring results; training and supervising crews; and job hazard analysis. Willing to take initiative and act as a team member to complete projects; and capable of communicating with project managers, team members, and the public.

GIS/Office Skills: Trained and experienced in the use of ArcGIS 10.7+ and a wide variety of extensions and tools, including spatial analyst. Additional training and experience with ArcPro, Quantum GIS, Python, SQL, and drafting programs such as AutoCAD and Sketchup. Experienced in the use of computers for producing written reports, data processing, research, and record keeping using the MS Office Suite (Word, Excel, Outlook, Publisher, and Access). I am also experienced in maintaining records of costs, expenses, and inventory.

Field and Data Collection Skills: Experienced using GPS and mobile computers and tablets including Trimble units to collect, edit, and display data with attention to detail and protocol. Over ten years of habitat restoration, conservation, and reforestation experience. Extensive field work in both urban and remote environments where safety protocol was of the highest priority.
______________________________________________________________________________________
Work History

Geospatial Analyst/GIS Specialist
Sanborn Maps contracted to,
Bureau of Land Management / Lakeview Resource Area January 2019 Current 2021
1302 South G Street 40 Hours per week
Lakeview, OR 97630 GS-9
This Geospatial Analyst position is responsible for GIS duties associated with producing the Resource Management Plan. Duties include but are not limited to, creating and managing Geodatabases, editing spatial data, working in a corporate data versioning environment, understanding and conforming to government spatial data standards, knowledge of land use/allocations stipulations, creating and managing FGDC compliant metadata, geo-processing both raster and vector based spatial data, remote sensing analysis, documenting analytical and workflow processes, cartographic production, and statistical analysis. Additional responsibilities involve communication and coordinating duties with project managers, planning coordinators, natural resource specialists, cartographic specialists, and data stewards.

Geographic Information System Technician
Hunter Communications August 2016 - December 2018
801 Enterprise Dr. #101 40 Hours per week
Central Point, OR 97502
The GIS Technician position performs mapping of fiber optic infrastructure, verifying as-built physical plant and pre-construction engineering data collection. Mapping and drafting occurs primarily with M4 mapping software though other Geographic Information System software (including ArcGIS and QGIS), and mobile applications are also utilized. The position also is responsible for generating reports, collecting field data on infrastructure; assist in producing cost estimates, and documenting Network Operations equipment and fiber optic circuit connections. Workflow utilizes data collection with GPS and mobile computer collection units that have laser rangefinder and stereoscope photography to measure infrastructure.

Start-up Manager/Auditor - Atmospheric Corrosion February 2016 - July 2016
Southern Cross Corp.
3175 Corners North Court NW 40 Hours per week
Peachtree Corners, GA 30071
Project involved working under contract with Avista Utilities to perform utility facility inspections in Southern Oregon. This position included using ArcGIS online, Access and Excel to produce reports and analyze performance and production: training and supervising survey technicians: as well as providing quality control, following safety guidelines and protocol, and completing required paperwork. Field technicians navigated to and inspect gas meters and facilities using Trimble Juno PDA and GPS units. This position was also responsible for management and troubleshooting of Trimble units, loading software and downloading field data to company web portal.

Field Representative November 2015- August 2016
Information Providers Inc. 24 Hours per week
33 10th Ave. Ste. 301
Hopkins, MN 55343
Conducted property insurance inspections in Southern Oregon for a national company. Field representative conducted inspections of dwellings for homeowner s insurance policies. The field representative traveled to insured properties and conducted inspections by recording detailed dwelling information (including construction type), assessing risks, and taking measurements and photos following inspection protocol. Inspections included measuring and diagramming dwellings with drafting software. Field representative entered case data and submitted diagrams via computer on company web portal.

Traffic Data Collector October 2015 November 2015
City of Medford - Public Works Dept. 12 Hours per week
441 W 8th St.
Medford, OR 97501
Position collected data on traffic usage at primary intersections throughout the city as part of annual Engineering / Development Division traffic study to support transportation planning and the mapping of traffic patterns. Part time position.

GIS Analyst/Survey Technician March 2015 - August 2015
Neathamer Surveying, Inc. 40 Hours per week
3126 State St. #203
Medford, OR 97504
Performed GIS, research and clerical duties for a surveying company, including researching parcel ownership, deed information, zoning requirements, county and municipal ordinances, relevant prior surveys, and other property related data. This position also helped prepare applications for submittal to planning departments, traffic control plans, and information to be used by field survey crews.

Forest Resource Survey Technician November 2014 - March 2015
K H Hewitt Forest Resources 40 Hours per week
3115 Racine Blvd.
Bellingham, WA 98226
Performed biological resource surveys including terrestrial mollusk surveys on BLM lands as part of a forest resource inventory used for compliance and planning. Used GPS to navigate to remote locations, record results, identify target species and collect samples. Position included producing GIS related data with ArcGIS programs for submittal to BLM in a Geodatabase format.

Biological Science Technician July 2014 - October 2014
Lava Beds National Monument 40 Hours per week
1 Indian Well Headquarters GS-5
Tulelake, CA. 96134
Created, edited, and maintained a geodatabase used to assess natural resource conditions. Collected data on tree health and height with ArcPad software and produced an annual report for an evaluation of hazardous trees. This position also assumed the direction of a road restoration and revegetation project, including monitoring results and producing a project report. Additional work involved researching scientific studies and producing a landscape management plan with defensible space vegetation management guidelines for residential areas in the monument.

Science Technician/SW Oregon Field Assistant February 2014 June 2014
The Nature Conservancy 40 Hours per week
33 N Central Ave. Ste. 405
Medford, OR 97501
Created, edited, and maintained a geodatabase used for analysis of oak habitats that included, plant association identification, estimated vegetation cover, aerial photo interpretation, and LIDAR classification of native habitats. Set up monitoring plots and collected oak habitat data with Trimble Juno units and ArcPad software. Data collection and management required the extensive use of ArcGIS mapping software, analysis tools, Access, and Excel. The position entailed working with staff, volunteers, the BLM, external parties and the public; to determine management guidelines for current and future habitat restoration efforts on the Table Rocks; an Area of Critical Environmental Concern in Southern Oregon. The position also assisted with preserve management including mapping and removal of invasive species; and recording management activities in the ArcGIS Server conservancy geodatabase.

Biological Science Technician July 2013 October 2013
Lava Beds National Monument 40 Hours per week
1 Indian Well Headquarters GS-5
Tulelake, CA. 96134
Mapped invasive plants and executed the annual exotic weed control program. Used ArcGIS programs and models to prioritize areas for treatment, prepare annual accomplishment reports, summarize technical information, and execute fieldwork in remote environments to generate new or verify mapped information. The position also collected and processed native plant seeds and assisted in the setup and operation of the native plant nursery.

Quality Control Auditor - Atmospheric Corrosion Technician February 2013 - July 2013
Southern Cross Corp. 40 Hours per week
3175 Corners North Court NW
Peachtree Corners, GA 30071
Project involved working under contract with Avista Utilities to perform utility facility inspections in Southern Oregon. This position included using ArcGIS online, Access and Excel to produce reports and analyze performance and production: training and supervising survey technicians: as well as providing quality control, following safety guidelines and protocol, and completing required paperwork. Field technicians navigated to and inspect gas meters and facilities using Trimble Juno PDA and GPS units. This position was also responsible for management and troubleshooting of Trimble units, loading software and downloading field data to company web portal.

Field Crew Member /GIS Mapping Tech October 2012 November 2012
The Nature Conservancy 40 Hours per week
647 Washington Street
Ashland, OR 97520
Surveyed, inventoried, and mapped trees, native vegetation, and forest stands with Trimble survey grade GPS units on USFS and BLM lands for forest resiliency and stand reconstruction studies sponsored by The Nature Conservancy and the US Forest Service. Work also included identifying tree species, coring trees to determine age, and recording the data in a database. Used Trimble Juno units with ArcPad to collect data on invasive species, oak habitats, and vernal pools on Nature Conservancy property in Southern Oregon. Identified both invasive species and native plants and used GPS to navigate to specific locations to treat invasive species and map restoration activities using ArcPad and ArcGIS programs.

Volunteer Conservation Work Party Member March 2012 - September 2012
The Nature Conservancy
647 Washington Street
Ashland, OR 97520
Use Trimble Juno with ArcPad to collect data on invasive species, oak habitat, vernal pools, and barbed wire fencing on Nature Conservancy property in southern Oregon. Used GPS to navigate to specific locations to remove invasive species and weeds either mechanically or by hand. Identified both invasive species and native plants.

____________________________________________________________________________________
Education
The Pennsylvania State University; Post Graduate GIS Certification Program
University of Oregon; Bachelor of Science; Geography/Environmental Studies

-----END OF RESUME-----

Resume Title: Oil

Name: Steve Harvey
Email: steveharvey300@yahoo.com
Phone: true
Location: Lakeview-OR-United States-97630

Work History

Job Title: Geospatial Analyst/GIS Specialist 01/01/2019 - Present
Company Name: Sanborn Maps; Bureau of Land Management / Lakeview Resource Area

Job Title: Geographic Information System Technician 08/01/2016 - 12/31/2018
Company Name: Hunter Communications

Job Title: Field Representative 11/01/2015 - 08/31/2016
Company Name: Information Providers Inc.

Job Title: Start-up Manager/Auditor - Atmospheric Corrosion 02/01/2016 - 07/31/2016
Company Name: Southern Cross Corp.

Job Title: Traffic Data Collector 10/01/2015 - 11/01/2015
Company Name: City of Medford - Public Works Dept.

Job Title: GIS Analyst/Survey Technician 03/01/2015 - 08/31/2015
Company Name: Neathamer Surveying, Inc.

Job Title: Forest Resource Survey Technician 11/01/2014 - 03/01/2015
Company Name: K H Hewitt Forest Resources

Job Title: Biological Science Technician 07/01/2014 - 10/31/2014
Company Name: Lava Beds National Monument

Job Title: Science Technician/SW Oregon Field Assistant 02/01/2014 - 06/30/2014
Company Name: The Nature Conservancy

Job Title: Biological Science Technician 07/01/2013 - 10/31/2013
Company Name: Lava Beds National Monument

Education

School: Pennsylvania State University, Major:
Degree: Doctorate, Graduation Date:

School: University of Oregon, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/17/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
RCM2Z677RTNN38FDCTR


