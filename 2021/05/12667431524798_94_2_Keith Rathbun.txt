KEITH R. RATHBUN
Olney, Maryland | 301-260-1905 | Keith.Rathbun123@gmail.com | https://www.linkedin.com/in/keith-rathbun
QUALIFICATIONS SUMMARY
Experienced and innovative systems professional with over 15 years of experience working on complex large-scale
software development and maintenance projects as both a director and developer. Produced web sites, user interfaces,
software, documentation, and reports for clients including DoD, CMS, DHHS, NASA, and DHS. Developed software
training and best practices guidelines. Supervised, mentored, and led large teams of technical staff.
SKILLS OVERVIEW

Project Management
System Development Lifecycle (SDLC)
Professional Scrum Master (Certified)
Visual Basic Programming
SAS Programming and Enterprise Guide
Spark SQL and Databricks Programming
HTML, Web Site Development
Relational Database Design & Development
Software Testing, Verification & Validation
Data Use Agreements (DUA)
Windows, Unix, Linux, DOS, and VMS

Leadership and Supervision
Data Management
Data Security, HIPAA
Data Validation
Data Analysis
Data Visualization
Software Best Practices & Guidelines
Budgeting and Planning
JIRA and Workflows
Configuration Management, GIT
Requirements Definition & Analysis

Technical Recruiting
Business Development, Proposals
Mainframe Programming, JCL
Microsoft Office Suite
Process Improvement & Automation
Staff Development and Training
Reverse Engineering
Medicare & Medicaid Health Data
Health Measure Development
Technical Writing
Quality Assurance

PROFESSIONAL EXPERIENCE AND SELECTED PROJECTS

TechSur Solutions, Washington, DC

February 2020 July 2020

Principal Data Analyst and SAS Consultant for Department of Homeland Security
Conducted correlation analyses on performance and efficiency metrics and created correlation matrices. Created
analysis files and data visualizations by office, immigration form, organization, and at the national level.
Wrote SAS programs to update the Systems of Records (SOR) operational database system and construct load
files for the Performance Reporting Tool (PRT).
Utilized Databricks and Apache Spark SQL to convert the SOR database system from SAS to Databricks.
Developed and modified SQL queries to improve efficiency and meet ongoing requirement changes.

Mathematica Policy Research, Washington, DC

1994 - 2018

Associate Director of Health Research Systems
Assisted with strategic planning to support the research division s objectives.
Wrote quality assurance plans, data management plans, and systems documentation.
Assisted with recruiting and provided staff coordination and supervision for a staff of systems analysts, senior
programmers, and research assistants/programmers.
Assisted with establishing computing standards and staff training, best practices, recruiting goals, supervisory
assignments, and security procedures.
Wrote winning proposals and provided budgeting support services for the research division.
Supervisor & Mentor of Senior Programmers and Systems Analysts
Assisted the director in activities to support training, mentoring, and supervision.
Coordinated and scheduled project assignments while monitoring workloads for systems analysts and senior
programmers. Served as mentor and adviser.
Wrote performance reviews, interviewed, and recruited candidates for programming and analysis positions.
Senior Systems Analyst, Health Care Survey of U.S. Department of Defense Beneficiaries: Sample and
Questionnaire Design, Analysis and Reporting
Served as SAS programming task leader for survey sample selection, response rate calculations, weighting,
database, and reporting programming tasks.
Designed and developed SAS programs and led the programming tasks.
Produced automated quarterly and annual survey reports in HTML format for DoD TRICARE Websites.
Wrote and provided DoD client with documentation such as annual and quarterly survey databases and
codebooks, technical manuals, and design reports.
Performed quality control, facilitated code reviews, and wrote technical sections of proposals.

�KEITH RATHBUN

PAGE 2

Senior Systems Analyst, Evaluation of the Independence At Home (IAH) Demonstration
Served as SAS programming/systems director and data custodian.
Designed the reporting system and led the programming team to produce data diagnostics and health quality
measures based on billions of Medicare/Medicaid claims records with a focus on the demonstration sites and
control group.
Coordinated data use agreements (DUAs) through CMS and the Virtual Research Data Center (VRDC).
Established standards, provided data management, HIPAA data security, and SAS/SQL programming training
support.
Produced annual and ad-hoc reporting results to assess the impact of IAH in providing comprehensive,
coordinated, continuous, and accessible care to high-need populations at home and coordinate health care
across all treatment settings.
Senior Systems Analyst, Bundled Payment for Care Improvement (BPCI)
Led a team of systems analysts/architects and programmers to analyze the existing SAS-based reporting system
and workflows and suggest and document system improvements.
Presented recommendations for process improvements/changes to senior management and project leadership.
Senior Systems Analyst, Medicaid and CHIP Business Information Solution (MACBIS)
Developed data management plan for the MACBIS Scorecard.
Helped define data requirements, create data visualizations, and led analysts and software developers to create
website to graphically depict how Medicaid and CHIP are performing and shared this rich data with stakeholders.
Senior Systems Analyst, Development of Resource Use Reports for Medicare Fee-for-Service Claims
Served as the parallel programming task leader on the development and calculation of the value-based payment
modifier and production of the Quality and Resource Use Reports for Physician Feedback project.
Reviewed specifications and programming work and assisted with output reconciliation to ensure report quality.
Facilitated project staffing, created, and monitored budgets, and presented status reports at client meetings.
Senior Systems Analyst, TRICARE Standard Survey of Beneficiaries and Civilian Providers Analysis and
Presentation of Results
Provided programming and management support for the beneficiary, provider, and TRICARE For Life and
TRICARE Prime Remote surveys and reporting.
Prepared DUAs and system assurance documents and served as data custodian.
Wrote SAS programs and provided survey database and sample selection support.
Coordinated data transfer, data security, and encryption with subcontractors.
Led the effort to prepare and deliver database CDs consisting of codebooks, final data sets in a variety of formats,
frequency tables, standard errors, annotated questionnaires, and response rate tables for all beneficiary, provider,
and supplemental surveys.
Prepared budgets, assigned and monitor programming tasks, performed quality control, facilitated code reviews,
and wrote design reports and technical sections of proposals.
Senior Systems Analyst, Evaluation of Health Care Innovation Awards (HCIA) Round 2
Served as systems director for a large SAS programming team.
Led the data systems portion of the proposal effort and assisted with the budgeting, writing, and staffing plans.
Designed the reporting system used to produce site-specific data diagnostics and health quality measures
reporting such as outcomes and expenditures for the awardees.
Wrote data acquisition plan and led the effort to establish memorandums of understanding (MOUs) and Business
Associate Agreements (BAAs) with the 39 awardees. Developed and documented data transfer and security
procedures and provided training for the researchers and programmers.
Established CMS DUA and Virtual Research Data Center (VRDC) access for large team of programmers.
Trained the programmers on VRDC access, programming guidelines, and provided SAS programming support
and tips. Provided quality assurance support and facilitated programming specification and code reviews.
Senior Systems Analyst, Development, Management, and Support of the Scientists and Engineers Statistical Data
System (SESTAT)
Served as programming task leader for survey contractor specifications and data dissemination tasks.
Developed SAS programs to create contractor specifications and analyze and validate data for the National
Survey of Recent College Graduates, National Survey of College Graduates, and Survey of Doctorate Recipients
data sets.

�KEITH RATHBUN

PAGE 3

Developed SAS programs to create restricted and public use data and documentation CDs.
Utilized Visual Basic and designed and developed an Access database to manage and track the dissemination of
restricted use CDs. Wrote user s guide and data security/encryption procedures

Systems Analyst, Providing Technical Assistance for District of Columbia Temporary Assistance for Needy
Families (TANF) Vendors
Utilized Visual Basic and designed and developed MS Access TANF reporting relational database for 14 TANF
vendors, wrote User s Guides documenting these databases, provided technical support and training, installed
software, and enhanced software to support the client s and vendor s ever-changing payment and billing
requirements. Databases were used to manage and track payments for the TANF vendors customers.

Computer Sciences Corporation, Laurel, MD

1988 - 1994

Programmer/Analyst
Served as programming task leader for the Astrometry and Engineering Data Processing (AEDP) System and the
Payload Operations Control Center Applications Software Support (PASS) System of the Hubble Space
Telescope (HST) Ground System. Wrote FORTRAN programs and managed the configuration, integration, and
testing of data management and data processing programs to support upgrades for the HST ground system.
Presented design materials at the Goddard Space Flight Center for software enhancements. Provided operations
support and training and revised documentation such as requirements, user s guides, and interface control
documents.
As Staff Writer, wrote articles for the Control Systems Technology Group newsletter.
EDUCATION
Bachelor of Science, Information Systems Management
Bachelor of Arts, Economics
University of Maryland, Baltimore County, Maryland
Associate of Arts, Computer Science
Montgomery College, Rockville, Maryland
CERTIFICATIONS AND AWARDS
Professional Scrum Master, Scrum.org
HST Launch Support Group Achievement Award
Excellence Award for Quality and Productivity, Goddard Space Flight Center
Public Service Group Achievement Award, NASA

�

-----END OF RESUME-----

Resume Title: Technology - Developer, Programmer, Engineer

Name: Keith Rathbun
Email: keith.rathbun123@gmail.com
Phone: true
Location: Olney-MD-United States-20830

Work History

Job Title: Senior Systems Analyst, Bundled Payment for Care Improvement (BPCI) 01/01/1900 - 04/26/2021
Company Name: true

Job Title: Principal Data Analyst and SAS Consultant for Department of Homeland Security 02/01/2020 - 07/31/2020
Company Name: TechSur Solutions

Job Title: Associate Director of Health Research Systems 01/01/1994 - 12/31/2018
Company Name: Mathematica Policy Research

Job Title: Programmer/Analyst 01/01/1988 - 12/31/1994
Company Name: Computer Sciences Corporation

Education

School: true, Major:
Degree: Bachelor's Degree, Graduation Date:

School: University of Maryland, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Montgomery College, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/26/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Information Technology
Internet-E-Comerce

Downloaded Resume from CareerBuilder
RD95YC6J5BZ8QLTLJWF


