DEVI DOSHI, R.N.
(732) 725-2517 DeviDoshi824@gmail.com

EXPERIENCE

Penn Presbyterian Medical Center, Philadelphia, PA

Registered Nurse PACU

December 2016-Present

Provide acute care for post-operative patients recovering from anesthesia
Assess patient readiness for discharge home rehabilitation centers or in-hospital wings & communicate
discharge instructions verbally and in writing ensuring patient and/or caretaker understanding of post-op care
Educate patients and their caregivers on the proper use and safety of orthopaedic equipment such as braces,
immobilizers, casts, and crutches
Roxborough Memorial Hospital, Roxborough, PA
November 2919-July 2020

Registered Nurse PACU-Per Diem

Provide acute care for post-operative patients recovering from anesthesia
Assess patient readiness for discharge home rehabilitation centers or in-hospital wings & communicate
discharge instructions verbally and in writing ensuring patient and/or caretaker understanding of post-op care
Educate patients and their caregivers on the proper use and safety of orthopaedic equipment such as braces,
immobilizers, casts, and crutches
The Children s Hospital of Philadelphia, Philadelphia, PA
July 2015-November 2018

Critical Care Registered Nurse-Surgical/Medical/Trauma PICU

Provide critical care nursing in the intensive care unit for patients with acute & chronically ill diseases; ages
varying from neonates to adults
Initiate & maintain continuous hemodynamic monitoring; safely administer medications and monitor
vasoactive drips, sedation, insulin drips, transparental nutrition, blood product transfusions, intravenous fluids,
electrolytes, & antibiotics
Monitor & sustain ventilated patients, tracheostomies, arterial lines, central venous pressure lines,
ventriculostomies, nephrotic tubes, chest tubes, internal drains, continuous veno-venous hemofiltration &
peritoneal dialysis
Facilitate family based care on communication between doctors, families, child life specialists, therapists,
social workers & case management

Jersey City Medical Center, Jersey City, NJ (Level ll Trauma Center)

Critical Care Registered Nurse-Surgical/Medical/Trauma/Cardiac ICU

Initiated & maintained continuous hemodynamic monitoring for up to three critically ill patients per shift
Safely administered medications and monitored vasoactive drips, sedation, insulin drips, transparental
nutrition, blood product transfusions, intravenous fluids, electrolytes, & antibiotics
Monitored & sustained ventilated patients, tracheostomies, arterial lines, central venous pressure lines,
ventriculostomies, nephrotic tubes, chest tubes, & internal drains

Integrated Nursing Associates, Lake Hiawatha, NJ

Registered Nurse-Pediatric Population Only

Registered Nurse-Outpatient

November 2012-June 2013

Provided direct nursing care in a home/school setting for pediatric patients with a multitude of illnesses
Regularly re-evaluated the patient's health needs, initiated the plan of care and necessary revisions, initiated
appropriate preventive and rehabilitative nursing procedures

Broad Street Medical Associates, Clifton, NJ

April 2013-June 2015

June 2010-June 2013

Prepared patients for examinations, obtained vital signs and medical histories, assisted physician during
examinations, performed procedures within my scope of practice, and all customary skills & nursing duties
Assessed & triaged patients to determine which needs were most critical to health
Trained staff on transitioning from paper to electronic charting of eClinical Works Software

EDUCATION

Seton Hall University, College of Nursing, South Orange, NJ
Bachelors Science in Nursing (R.N. BSN)

December 2011

�Wilmington University, College of Health Professions, Wilmington, DE
Masters in Nursing Education (M.S.N)

SKILLS
Computer:
Languages:
Medical:
Other:

March 2020- December 2021

Microsoft Word, Excel, PowerPoint, eClinical Works Electronic Medical Records Software, Siemens
Soarian Clinical Electronic Record Software, EPIC Electronic Records
English, Gujarati
PA R.N. Licensed, TNCC, BLS, ACLS, PALS, IV therapy certification (adult & pediatric), NJ EMT
certified, CVVH, Peritoneal Dialysis, numerous continuing education credit classes to maintain EMS &
RN Certifications
Strong interpersonal and communication skills that enhance patient-focused care & increase
communication with leadership, administration, patients, & families, ability to manage multiple tasks
with effective time management

ACTIVITES

Secretary, Teaneck Volunteer Ambulance Corps (2010 - 2015)
EMT, Teaneck Volunteer Ambulance Corps (2009-2015)
Scout Ambassador, Girl Scouts of USA, Troop 569
Member of American Association of Critical Care Nurses (AACN)

�

-----END OF RESUME-----

Resume Title: Education

Name: Devi Doshi
Email: devidoshi824@gmail.com
Phone: true
Location: Philadelphia-PA-United States-19101

Work History

Job Title: Registered Nurse PACU 12/01/2016 - Present
Company Name: Penn Presbyterian Medical Center

Job Title: Registered Nurse PACU-Per Diem 07/19/2020 - 05/24/2021
Company Name: Roxborough Memorial Hospital

Job Title: true 01/01/1900 - 05/24/2021
Company Name: Jersey City Medical Center

Job Title: Critical Care Registered Nurse-Surgical/Medical/Trauma PICU 07/01/2015 - 11/30/2018
Company Name: The Children s Hospital of Philadelphia

Job Title: true 04/01/2013 - 06/30/2015
Company Name: Broad Street Medical Associates

Job Title: Registered Nurse-Pediatric Population Only Registered Nurse-Outpatient 11/01/2012 - 06/30/2013
Company Name: Integrated Nursing Associates

Education

School: Seton Hall University, College of Nursing, South Orange, NJ, Major:
Degree: Bachelor's Degree, Graduation Date:

School: Wilmington University, College of Health Professions, Wilmington, DE, Major:
Degree: Master's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: Graduate Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/23/2021 12:00:00 AM

Desired Shift Preferences:

Interests:
Nurse

Downloaded Resume from CareerBuilder
R3100S61RLHRKG0V5QC


