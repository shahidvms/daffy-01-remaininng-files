Alesandra Arias
2421 Crotona Avenue Bronx, NY 10458
Bronx, NY 10458
alesandramarias9@gmail.com
(917)5576103

An enthusiastic business professional with three years of receptionist experience while providing
excellent customer service to daily guests. Looking to utilize my skills while obtaining a position as a
member in a people-oriented organization.

Willing to relocate: Anywhere
Authorized to work in the US for any employer

Work Experience

Communications Associate - Intership
State of Mind LLC - Brooklyn, NY
September 2020 to October 2020
Train and develop team members, participate in management meetings and business trips about the
company culture and systems. Maintain a clean and productive office environment. Selling organic
products around the world providing exceptional sales and marketing skills with customers. Manage
5-10 associates.

Business Operations Associate
Wyndham Destinations - Manhattan, NY
September 2016 to September 2020
Maintained a positive attitude at all times with each guest while checking them in. During every shift,
ensured every guest that checked in was connected to a sales representative. Answered calls as well
as guest questions professionally. Carefully entered data into systems such as CRS, Prime, Salespoint
and Journey using Citrix daily. Accurately managed and distributed daily inventory of all premiums
and cash given ranging from $4 to $1000. Ran daily reports at the end of shift to ensure each gift was
accounted for. Handled proprietary information in a confidential and classified manner.

Sales Associate
H&M Retail Company - Manhattan, NY
March 2014 to August 2014
Greeted all guests entering the store while maintaining a positive and welcoming attitude. Maintained
a clean and organized store. Kept track of all inventory that came in and out of the warehouse.
Used down time to efficiently fold clothing, clean fitting rooms, and organize every display. Ensured
the presence of the sales floor was always neat and inviting. Counted the cash registers during the
beginning and end of every shift ensuring all merchandise was accounted for and money totals were
always correct.

Education
High school diploma
Fannie Lou Hamer Freedom School - Bronx, NY
September 2009 to June 2013

Associate in Hospitality Management
Hostos Community College - Bronx, NY
January 2017

Skills
Team Building (8 years)
Time Management
Expert Database Management
Expert Administrative
Expert Typing
Spanish (10+ years)
Citrix
Microsoft Windows
Microsoft SharePoint
Communications
Salesforce
Leadership Experience
Operating Systems

-----END OF RESUME-----

Resume Title: Communications Associate

Name: alesandra arias
Email: alesandramarias9@gmail.com
Phone: (917) 557-6103
Location: New York-NY-United States-10016

Work History

Job Title: Communications Associate 09/01/2020 - 10/31/2020
Company Name: State of Mind LLC

Job Title: Business Operations Associate 09/01/2016 - 09/01/2020
Company Name: Wyndham Destinations

Job Title: Sales Associate 03/01/2014 - 08/31/2014
Company Name: H&M Retail Company

Education

School: Fannie Lou Hamer Freedom School - Bronx, NY, Major:
Degree: High School, Graduation Date:

School: Hostos Community College - Bronx, NY, Major:
Degree: None, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken:
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: ["Full Time","Part Time","Intern","Seasonal","Temporary","Contractor"]
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: None
Certifications:
Employment Type:
Military Experience:
Last Updated: 4/7/2021 3:54:16 PM

Desired Shift Preferences:

Interests:
Arts - Culture

Downloaded Resume from CareerBuilder
R2P1BJ6M35HZ13BQRCK


