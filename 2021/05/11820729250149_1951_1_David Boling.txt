RESUME
D.RobBoling
817-201-7856
bolingdr@gmail.com

SummaryofExperience
Oil&GasIndustryProfessionalandhighlyadaptablebusinessleaderwithmorethantwentyyearsofmulti-facetedland,title
and right-of-way experience, gaining extensive best-practice knowledge, from face-to-face landowner negotiations up to
Fortune500CompanyProcedureInception&Implementation.

ProfessionalPositions
FullstreamEnergyHoldings,LLC. DirectorofLand&GIS/Jan2019 Mar2021

DirectedallLand/Title/ROWactivitiesforanemergingmidstreamcompanyintheMarcellusbasin.Memberofhighlydriven,
effectiveteammanaginganannualCAPEXof$150Million.
- Managementofallright-of-way,duediligence,andmidstreamlandassets
- Implementationofnewtechnologies/processes-DroneSurveyIntegrationandArcGISOnlinePortal
- Managementandsynchronizationoftitle,landrecordsandArcGISmappingdata
- Oversightofservicecompanies(Procurement/ContractManagementofSurvey,Environmental,Right-of-Way,
Construction,andReclamation)
- LegalDisputeManagement/Mitigation

EnergyResourcePartners,Inc. Manager,VPOperations,Partner/Nov2007 Dec2018

Responsibleforallaspectsofright-of-wayprojectmanagement,contractorsupervision,reporting,schedulingofpipelinetitle,
and surface land rights acquisition projects in AL, FL, TX, OH, PA, WV for clients: JUWI, Williams, Sunoco, Energy Transfer,
Chesapeake,andAccessMidstream.
- Acquisitionofsurfacesites,easements,feesimpleproperty,minerals,androyalty
- Surfaceandmineraltitleabstracting,reviewandcurative
- Permittingwithalllocalandstategovernmentalagencies
- Coordinationofsurveyors,constructionandtitle/right-of-wayagents
- Securingwatercontractsfromstate,localandprivatesources
- ArcGISmappingofassetswithsupporttoClient sprojectmanagers
- QuickBooks,payrollprocessing,accounting

WilliamsProductionGulfCoastCo.LLC Right-of-WaySupervisor/Aug2005 Oct2007

Projectmanagementandsupervisionofallfunctionsofland,leasing,andpipelineright-of-wayprojectsintheBarnettShale
countiesofTexas.
- Negotiationsforacquisitionanddivestitureofpipelineeasements,assetsandfeeownedrealestate
- ProjectmanagementreportingtoDirectorofFacilities,Operations,LandandEH&S.
- CoordinationwithnumerouscitymunicipalitiesforTCEQandspecialusepermits
- Supervisingbrokers,crewsandagentsthroughallphasesofprojects

LandTitleResources,Inc. Owner,President/Jan1998 July2005

FullservicemineralandsurfacetitleabstractingfortheOilandGasindustryandRealEstateTitleAttorneys.
- Performedcertifiedtitleabstractsin40+LouisianaParishes.
- Createdacustomweb-baseddatabasetomanageover20,000titleabstracts,and25+abstractors/employees.

Education
UniversityofLouisianaatLafayette
StudiesincludedElectricalEngineering&ComputerScience

References
BrianEaster-Director,Eng.&Const. FullstreamEnergy
TaylorJames-DirectorofOperations Williams

EliottPorter-LeadSpecialist,Land&ROW EnergyTransfer

412-605-4958
412-328-4911
878-332-2218

�

-----END OF RESUME-----

Resume Title: Oil/Energy/Power - Operations

Name: David Boling
Email: bolingsr@gmail.com
Phone: true
Location: Dallas-TX-United States-75224

Work History

Job Title: Director of Land & GIS 01/01/2019 - 03/31/2021
Company Name: Fullstream Energy Holdings, LLC.

Job Title: Manager, VP Operations, Partner 11/01/2007 - 12/31/2018
Company Name: Energy Resource Partners, Inc.

Job Title: Supervisor 08/01/2005 - 10/31/2007
Company Name: Williams Production Gulf Coast Co. LLC

Job Title: Owner, President 01/01/1998 - 07/31/2005
Company Name: Land Title Resources, Inc.

Education

School: University of Louisiana at Lafayette, Major:
Degree: Bachelor's Degree, Graduation Date:

Additional Skills And Qualifications

Most Recent Job Title: true
Most Recent Pay: USD per
Managed Others: true
Languages Spoken: English
Work Authorization: Not Specified
Security Clearance: No
Felony Conviction: No

Desired Pay: USD per
Desired Job Types: Full Time
Travel Preference:

Currently Employed: No
Jobs Last Three Years: 0
Last Job Tenure Months: 0
Highest Degree: 4 Year Degree
Certifications:
Employment Type:
Military Experience:
Last Updated: 5/11/2021 12:00:00 AM

Desired Shift Preferences:

Interests:

Downloaded Resume from CareerBuilder
RDD8306MD130J5RD1VV


